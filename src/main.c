#include <pebble.h>
Window *base_window;
Layer *background_cloud;
Layer *left_cloud;
Layer *right_cloud;
Layer *sun;
TextLayer *time_in_cloud;
TextLayer *battery_layer;
TextLayer *connected_layer;
TextLayer *am_pm_layer;
TextLayer *current_date;
GColor window_bg_color, percent_color;
PropertyAnimation *sun_animation;
static char current_time[] = "12:56", am_pm[] = "AM", date[] = "12/12/12", battery_percent_char[] = "100%";
int current_hour_int;

bool afternoon_is_true() {
	if (current_hour_int == 13 || current_hour_int == 14 || current_hour_int == 15 || current_hour_int == 16 || current_hour_int == 17 || current_hour_int == 18
		|| current_hour_int == 19 || current_hour_int == 20 || current_hour_int == 21 || current_hour_int == 22 || current_hour_int == 23 || current_hour_int == 24) {
			return true;
		} else {
			return false;
		}
}

void battery_handler (BatteryChargeState bcs) {
	int battery_percentage = bcs.charge_percent;
	bool battery_charging = bcs.is_charging;
	
	snprintf(battery_percent_char, sizeof(battery_percent_char), "%d%%", battery_percentage);
	text_layer_set_text(battery_layer, battery_percent_char);
	
	
	if (battery_charging) {
		text_layer_set_text(connected_layer, "Charging");
		
		if (afternoon_is_true()) {
			text_layer_set_text_color(connected_layer, GColorBrightGreen);
		} else {
			text_layer_set_text_color(connected_layer, GColorDarkGreen);
		}

	} else {
		text_layer_set_text(connected_layer, "Not Charging");
		
		if (afternoon_is_true()) {
			text_layer_set_text_color(connected_layer, GColorIcterine);
		} else {
			text_layer_set_text_color(connected_layer, GColorFolly);
		}

	}

	if (battery_percentage > 60) {
		if (afternoon_is_true()) {
			window_bg_color = GColorFromRGB(30, 114, 114);
			percent_color = GColorFromRGB(111, 245, 59);
		} else {
			window_bg_color = GColorFromRGB(85, 170, 170);
			percent_color = window_bg_color;
		}
	} else if (battery_percentage > 30) {
		if (afternoon_is_true()) {
			window_bg_color = GColorFromRGB(114, 114, 4);
			percent_color = GColorFromRGB(227, 193, 0);
		} else {
			window_bg_color = GColorFromRGB(170, 170, 1);
			percent_color = window_bg_color;
		}

	} else {
		if (afternoon_is_true()) {
			window_bg_color = GColorFromRGB(156, 16, 16);
			percent_color = GColorFromRGB(232, 29, 242);
		} else {
			window_bg_color = GColorFromRGB(255, 85, 0);
			percent_color = window_bg_color;
		}

	}
	
	window_set_background_color(base_window, window_bg_color);
	text_layer_set_text_color(battery_layer, percent_color);
	
}

void timer_handler(struct tm *t, TimeUnits time_changed) {
	strftime(current_time, sizeof(current_time), "%I:%M", t);
	strftime(am_pm, sizeof(am_pm), "%p", t);
	strftime(date, sizeof(date), "%m/%d/%g", t);
	
	text_layer_set_text(time_in_cloud, current_time);
	text_layer_set_text(am_pm_layer, am_pm);
	text_layer_set_text(current_date, date);
	
	current_hour_int = t->tm_hour;

	GRect current_frame = layer_get_frame(sun);
	GRect next_frame;
	
	if(current_hour_int == 1 || current_hour_int == 7 || current_hour_int == 13 || current_hour_int == 19) {
		next_frame = GRect(-30, -17, 100, 100);
		
		sun_animation = property_animation_create_layer_frame(sun, &current_frame, &GRect(140, 140, 100, 100));
		animation_set_duration((Animation*)sun_animation, 1000);
		animation_schedule((Animation*)sun_animation);
		
		PropertyAnimation *go_to_one = property_animation_create_layer_frame(sun, &GRect(-80, -60,100, 100), &next_frame);
		animation_set_duration((Animation*)go_to_one, 1000);
		animation_set_delay((Animation*)go_to_one, 1000);
		animation_schedule((Animation*)go_to_one);
		
		return;
	} else if(current_hour_int == 2 || current_hour_int == 8 || current_hour_int == 14 || current_hour_int == 20) {
		next_frame = GRect(-6, 5, 100, 100);
	} else if(current_hour_int == 3 || current_hour_int == 9 || current_hour_int == 15 || current_hour_int == 21) {
		next_frame = GRect(18, 27, 100, 100);
	} else if(current_hour_int == 4 || current_hour_int == 10 || current_hour_int == 16 || current_hour_int == 22) {
		next_frame = GRect(42, 39, 100, 100);
	} else if(current_hour_int == 5 || current_hour_int == 11 || current_hour_int == 17 || current_hour_int == 23) {
		next_frame = GRect(66, 61, 100, 100);
	} else if(current_hour_int == 6 || current_hour_int == 12 || current_hour_int == 18 || current_hour_int == 24) {
		next_frame = GRect(90, 97, 100, 100);
	}
	
	sun_animation = property_animation_create_layer_frame(sun, &current_frame, &next_frame);
	animation_set_duration((Animation*)sun_animation, 2000);
	animation_schedule((Animation*)sun_animation);
	
}

void create_battery_cloud(Layer *layer, GContext *ctx) {
	if (afternoon_is_true()) {
		graphics_context_set_fill_color(ctx, GColorFromRGBA(68, 80, 128, 199));
	} else {
		graphics_context_set_fill_color(ctx, GColorFromRGBA(255, 255, 255, 199));
	}

	graphics_fill_rect(ctx, GRect(0, 0, 70, 40), 4, GCornersAll);
}

void create_charge_cloud(Layer *layer, GContext *ctx) {
	if (afternoon_is_true()) {
		graphics_context_set_fill_color(ctx, GColorFromRGBA(68, 80, 128, 199));
	} else {
		graphics_context_set_fill_color(ctx, GColorFromRGBA(255, 255, 255, 199));
	}

	graphics_fill_rect(ctx, GRect(0, 0, 85, 20), 4, GCornersAll);
}

void create_sun_update(Layer *layer, GContext *ctx) {
	if (afternoon_is_true()) {
		graphics_context_set_fill_color(ctx, GColorFromRGB(242, 228, 172));
	} else {
		graphics_context_set_fill_color(ctx, GColorFromRGB(255, 170, 0));
	}

	graphics_fill_circle(ctx, GPoint(50, 50), 40);
}

void cloud_process(Layer *layer, GContext *context_window) {
	if (afternoon_is_true()) {
		graphics_context_set_fill_color(context_window, GColorFromRGBA(68, 80, 128, 204));

		text_layer_set_text_color(current_date, GColorWhite);
		text_layer_set_text_color(am_pm_layer, GColorWhite);
		text_layer_set_text_color(time_in_cloud, GColorWhite);
	} else {
		graphics_context_set_fill_color(context_window, GColorFromRGBA(255, 255, 255, 204));

		text_layer_set_text_color(current_date, GColorBlack);
		text_layer_set_text_color(am_pm_layer, GColorBlack);
		text_layer_set_text_color(time_in_cloud, GColorBlack);
	}
	graphics_fill_rect(context_window, GRect(0, 0, 90, 46), 6, GCornersAll);
}

void sunrise_load(Window *window) {
	Layer *window_layer = window_get_root_layer(window);
	
	GFont proxima_nova_large = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_PROXIMA_NOVA_REG_26));
	GFont proxima_nova_small = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_PROXIMA_NOVA_REG_11));
	GFont date_font = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_ROBOTO_MEDIUM_12));
	
	sun = layer_create(GRect(25, 55, 100, 100));
	layer_set_update_proc(sun, create_sun_update);
	layer_add_child(window_layer, sun);
	
	left_cloud = layer_create(GRect(-4, 120, 90, 20));
	layer_set_update_proc(left_cloud, create_charge_cloud);
	layer_add_child(window_layer, left_cloud);
	
	right_cloud = layer_create(GRect(85, 70, 75, 130));
	layer_set_update_proc(right_cloud, create_battery_cloud);
	layer_add_child(window_layer, right_cloud);
	
	battery_layer = text_layer_create(GRect(79, 71, 72, 80));
	text_layer_set_background_color(battery_layer, GColorClear);
	text_layer_set_text_alignment(battery_layer, GTextAlignmentCenter);
	text_layer_set_font(battery_layer, proxima_nova_large);
	layer_add_child(window_layer, text_layer_get_layer(battery_layer));
	
	connected_layer = text_layer_create(GRect(-2, 122, 85, 20));
	text_layer_set_background_color(connected_layer, GColorClear);
	text_layer_set_font(connected_layer, date_font);
	text_layer_set_text_alignment(connected_layer, GTextAlignmentCenter);
	layer_add_child(window_layer, text_layer_get_layer(connected_layer));
	
	background_cloud = layer_create(GRect(29, 10, 90, 46));
	layer_set_update_proc(background_cloud, cloud_process);
	layer_add_child(window_layer, background_cloud);
		
	time_in_cloud = text_layer_create(GRect(20, 10, 90, 46));
	text_layer_set_text_alignment(time_in_cloud, GTextAlignmentCenter);
	text_layer_set_font(time_in_cloud, proxima_nova_large);
	text_layer_set_background_color(time_in_cloud, GColorClear);
	text_layer_set_text_color(time_in_cloud, GColorBlack);
	layer_add_child(window_layer, text_layer_get_layer(time_in_cloud));
	
	am_pm_layer = text_layer_create(GRect(99, 25, 20, 20));
	text_layer_set_background_color(am_pm_layer, GColorClear);
	text_layer_set_font(am_pm_layer, proxima_nova_small);
	layer_add_child(window_layer, text_layer_get_layer(am_pm_layer));
	
	current_date = text_layer_create(GRect(29, 39, 90, 20));
	text_layer_set_text_alignment(current_date, GTextAlignmentCenter);
	text_layer_set_background_color(current_date, GColorClear);
	text_layer_set_font(current_date, date_font);
	layer_add_child(window_layer, text_layer_get_layer(current_date));

	BatteryChargeState bcs = battery_state_service_peek();
	battery_handler(bcs);
}

void sunrise_unload() {
	layer_destroy(background_cloud);
	layer_destroy(left_cloud);
	layer_destroy(right_cloud);
	layer_destroy(sun);
	text_layer_destroy(time_in_cloud);
	text_layer_destroy(battery_layer);
	text_layer_destroy(connected_layer);
	text_layer_destroy(am_pm_layer);
	text_layer_destroy(current_date);
	property_animation_destroy(sun_animation);
}

void init() {
	base_window = window_create();
	
	tick_timer_service_subscribe(MINUTE_UNIT, timer_handler);	
	battery_state_service_subscribe(battery_handler);
	
	window_set_window_handlers(base_window, (WindowHandlers) {
		.load = sunrise_load,
		.unload = sunrise_unload
	});

	window_stack_push(base_window, true);
	
}

void deinit() {
	window_destroy(base_window);
	battery_state_service_unsubscribe();
	tick_timer_service_unsubscribe();
}

int main() {
	init();
	app_event_loop();
	deinit();
}